from flask import Flask, render_template, request


app = Flask(__name__)

@app.route('/<path:input>')
def fileCheck(input):
    if (input == "trivia.html" or input == "trivia.css"):
    	return render_template(input), 200
    elif input.startswith("..") or input.startswith("~"):
    	return render_template("403.html"), 403 
    else:
    	return render_template("404.html"), 404
    	
        

#@app.errorhandler(403)
#def error_403(error):
	#return render_template("403.html"), 403

#@app.errorhandler(404)
#def error_404(error):
	#return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
